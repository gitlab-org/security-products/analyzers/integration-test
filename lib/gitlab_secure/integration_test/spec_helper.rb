# Extract the full rspec example name which we can then use as the directory name to store the generated report files.
#
# When this file has been required by a spec file, for example:
#
# require 'gitlab_secure/integration_test/spec_helper'
#
# The following two variables will be made available:
#
# @description (deprecated)
#
#   Note:    @description is included for backwards compatibility, however, it has the potential to create directory
#            names that exceed the maximum length. For example, on OS X, the max file/directory name length is 255 characters.
#   Example: running-image-with-test-project-with-jsnpm-with-lockfile-v1-behaves-like-successful-scan-creates-a-report
#
# @output_dir
#
#   Note:    In order to reduce the likelihood of producing an output directory name that exceeds the maximum length, this
#            variable includes path separator characters between example_group names.
#   Example: running-image/with-test-project/with-jsnpm/with-lockfile-v1/behaves-like-successful-scan/creates-a-report
#
RSpec.configure do |config|
  config.before(:example) do |x|
    @description = full_description(x.metadata)
    @output_dir = output_dir(x.metadata)
  end

  # we need to override these patterns, because the default exclusion patterns
  # end up filtering out every line, which then causes rspec to output the
  # _full backtrace_ which is much too verbose.
  # See https://rspec.info/features/3-13/rspec-core/configuration/backtrace-exclusion-patterns/ for more details
  config.backtrace_exclusion_patterns = [
    /\/lib\d*\/ruby\//,
  ]
end

# remove any characters from the path which might cause issues when used as filenames
def sanitize_path(path)
  path.downcase.strip.gsub(/[^0-9a-z_\-\s]/, '').squeeze(" ").gsub(" ", "-")
end

def full_description(metadata)
  sanitize_path(metadata[:full_description] || '')
end

def output_dir(metadata)
  description = sanitize_path(metadata[:description] || '')

  if metadata.has_key?(:example_group)
    parent_description = output_dir(metadata[:example_group])
    return "#{parent_description}/#{description}"
  elsif metadata.has_key?(:parent_example_group)
    parent_description = output_dir(metadata[:parent_example_group])
    return "#{parent_description}/#{description}"
  else
    return description
  end
end
