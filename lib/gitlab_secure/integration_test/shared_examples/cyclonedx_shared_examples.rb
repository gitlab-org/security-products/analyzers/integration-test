require "rspec"
require "json"
require "rspec/json_expectations"
require "json_schemer"
require_relative "../cyclonedx_schema_path_resolver"
require_relative "./shared_functions"

require_relative "../../../../monkeypatches/rspec-json_expectations-monkeypatch.rb"

RSpec.shared_examples "non-empty CycloneDX files" do
  it "exists" do
    assert_paths_exist(relative_sbom_paths)
  end
end

# TOOD: remove this as part of https://gitlab.com/gitlab-org/gitlab/-/issues/370402
RSpec.shared_examples "expected CycloneDX metadata tool-name" do |tool_name|
  it "matches the expected tool name" do
    relative_sbom_paths.each do |relative_sbom_path|
      actual_sbom_path = File.join(scan.target_dir, relative_sbom_path)
      parsed_actual = JSON.parse(File.read(actual_sbom_path))

      expect(parsed_actual["metadata"]["tools"][0]["name"]).to eql(tool_name)
    end
  end
end

RSpec.shared_examples "recorded CycloneDX files" do
  it "matches expectations" do
    assert_files_match_expectations(expectations_dir, relative_sbom_paths, relative_expectation_dir, method(:sanitize_sbom))
  end
end

RSpec.shared_examples "valid CycloneDX files" do
  it "validates the schema" do
    relative_sbom_paths.each do |relative_sbom_path|
      actual_sbom_path = File.join(scan.target_dir, relative_sbom_path)
      parsed_actual = JSON.parse(File.read(actual_sbom_path))
      schema = JSON.parse(File.read(GitlabSecure::IntegrationTest::SchemaPathResolver::CycloneDX.new(parsed_actual).path))

      expect(JSONSchemer.schema(schema).validate(parsed_actual).to_a).to eql([])
    end
  end
end

def sanitize_sbom(json)
  cloned_json = Marshal.load(Marshal.dump(json))

  cloned_json.tap do |json|
    json.delete("serialNumber")
    if json.dig("metadata", "tools").is_a?(Array)
      json["metadata"]["tools"].first.delete("version")
      json["metadata"]["tools"].first.delete("name")
    end
    json["metadata"].delete("timestamp") if json.dig("metadata")
  end
end
