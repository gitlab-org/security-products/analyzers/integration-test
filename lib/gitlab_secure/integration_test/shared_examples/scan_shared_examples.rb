require "rspec"

RSpec.shared_examples "successful scan" do
  it "creates a report" do
    # debug scan when report is missing
    unless File.exist?(scan.report_path)
      puts
      puts "REPORT IS MISSING!"
      puts "expected report path: #{scan.report_path}"
      puts "exit code: #{scan.exit_code}"
      puts "combined output of the analyzer:"
      puts  scan.combined_output
      puts
    end

    expect(File).to exist(scan.report_path)
  end

  describe "exit code" do
    specify { expect(scan.exit_code).to eq 0 }
  end

  it "logs analyzer name and version" do
    expect(scan.combined_output).to match /\[INFO\].*analyzer v/
  end

  it "logs no error" do
    expect(scan.combined_output).not_to match /\[(ERRO|FATA)\]/
  end
end

RSpec.shared_examples "crashed scan" do
  it "creates no report" do
    expect(File).not_to exist(scan.report_path)
  end

  describe "exit code" do
    specify { expect(scan.exit_code).not_to eq 0 }
  end
end

RSpec.shared_examples "failed scan" do
  it "creates no report" do
    expect(File).not_to exist(scan.report_path)
  end

  describe "exit code" do
    specify { expect(scan.exit_code).not_to eq 0 }
  end

  it "logs an error" do
    expect(scan.combined_output).to match /\[(ERRO|FATA)\]/
  end
end
