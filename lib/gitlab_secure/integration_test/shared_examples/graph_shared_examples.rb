# frozen_string_literal: true

require 'rspec'

RSpec.shared_examples 'provides an export on failure' do
  describe 'exported files' do
    it 'produces non-empty files' do
      filesizes = GitlabSecure::IntegrationTest::GraphqlUtils.filesizes_from_match_filenames(artifact, filenames)
      # We matched files
      expect(filesizes).not_to be_empty
      filesizes.each do |filesize|
        # Files are not empty
        expect(Integer(filesize)).not_to eq 0
      end
    end
  end
end
