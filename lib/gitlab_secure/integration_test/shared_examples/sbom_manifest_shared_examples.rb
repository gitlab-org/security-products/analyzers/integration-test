require "rspec"
require "json"
require "rspec/json_expectations"
require_relative "./shared_functions"

RSpec.shared_examples "non-empty SBOM manifest" do
  it "exists" do
    assert_paths_exist([relative_sbom_manifest_path])
  end
end

RSpec.shared_examples "recorded SBOM manifest" do
  it "matches expectations" do
    assert_files_match_expectations(expectations_dir, [relative_sbom_manifest_path], relative_expectation_dir, method(:sanitize_sbom_manifest))
  end
end

def sanitize_sbom_manifest(json)
  cloned_json = Marshal.load(Marshal.dump(json))

  cloned_json.tap do |json|
    json.delete("timestamp")
    json["analyzer"].delete("version") if json.dig("analyzer")
  end
end
