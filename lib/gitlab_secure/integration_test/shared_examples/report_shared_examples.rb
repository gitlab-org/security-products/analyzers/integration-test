require "rspec"
require "json"
require "json_schemer"
require "rspec/json_expectations"

require_relative "../../../../monkeypatches/rspec-json_expectations-monkeypatch.rb"

require_relative "../comparable"
require_relative "../schema_path_resolver"
require_relative "./shared_functions"

RSpec.shared_examples "empty report" do
  describe "version" do
    specify do
      assert_report_exists

      expect(report["version"]).not_to be_empty
    end
  end

  describe "scan" do
    specify do
      assert_report_exists

      expect(report["scan"]).not_to be_empty
    end
  end

  describe "vulnerabilities" do
    specify do
      assert_report_exists

      expect(report["vulnerabilities"]).to be_empty
    end
  end

  describe "dependency_files" do
    specify do
      assert_report_exists

      expect(report["dependency_files"] || []).to be_empty
    end
  end
end

RSpec.shared_examples "non-empty report" do
  describe "version" do
    specify do
      assert_report_exists

      expect(report["version"]).not_to be_empty
    end
  end

  describe "scan" do
    specify do
      assert_report_exists

      expect(report["scan"]).not_to be_empty
    end
  end

  describe "vulnerabilities" do
    specify do
      assert_report_exists

      expect(report["vulnerabilities"]).not_to be_empty
    end
  end
end

RSpec.shared_examples "recorded report" do
  describe "scan" do
    subject { GitlabSecure::IntegrationTest::Comparable.scan(report["scan"]) }

    let(:expected) { GitlabSecure::IntegrationTest::Comparable.scan(recorded_report["scan"]) }

    it "is equivalent" do
      assert_report_exists

      expect(subject).to include_json(expected)
    end
  end

  describe "dependency_files" do
    subject { GitlabSecure::IntegrationTest::Comparable.dependency_files(report["dependency_files"]) }

    let(:expected) { GitlabSecure::IntegrationTest::Comparable.dependency_files(recorded_report["dependency_files"]) }

    it "is equivalent" do
      assert_report_exists

      expect(subject).to include_json(expected)
    end
  end

  describe "vulnerabilities" do
    subject { GitlabSecure::IntegrationTest::Comparable.vulnerabilities(report["vulnerabilities"]) }

    let(:expected) { GitlabSecure::IntegrationTest::Comparable.vulnerabilities(recorded_report["vulnerabilities"]) }

    it "is equivalent" do
      assert_report_exists

      expect(subject).to include_json(expected)
    end
  end

  describe "remediations" do
    subject { GitlabSecure::IntegrationTest::Comparable.remediations(report["remediations"]) }

    let(:expected) { GitlabSecure::IntegrationTest::Comparable.remediations(recorded_report["remediations"]) }

    it "is equivalent" do
      assert_report_exists

      expect(subject).to include_json(expected)
    end
  end
end

RSpec.shared_examples "valid report" do
  it "passes schema validation without errors" do
    assert_report_exists

    schema_path = GitlabSecure::IntegrationTest::SchemaPathResolver::SecurityReport.new(report).path
    schema = JSON.parse(File.read(schema_path))

    # set dependency_files field to an empty array if not set
    # because it's required in Dependency Scanning report schema v3.0.0 and later.
    # See https://gitlab.com/gitlab-org/gitlab/-/issues/300877 for details
    report["dependency_files"] ||= []

    validation_result = JSONSchemer.schema(schema).validate(report)

    validation_errors = validation_result.map { |error| JSONSchemer::Errors.pretty(error) }

    expect(validation_result.to_a).to be_empty, %|JSON validation against schema '#{schema_path}' failed with the following errors:\n\n| +
                                                %|  #{validation_errors.join("\n  ")}\n |
  end
end
