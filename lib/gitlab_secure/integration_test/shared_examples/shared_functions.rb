def assert_paths_exist(relative_file_paths)
  relative_file_paths.each do |relative_sbom_path|
    actual_sbom_path = File.join(scan.target_dir, relative_sbom_path)

    expect(File).to exist(actual_sbom_path), "Expected file '#{actual_sbom_path}' to exist"
  end
end

def assert_report_exists
  expect(File).to exist(scan.report_path), "Expected report '#{scan.report_path}' to exist"
end

def assert_files_match_expectations(expectations_dir, relative_file_paths, relative_expectation_dir, sanitize_function)
  relative_file_paths.each do |relative_file_path|
    actual_file_path = File.join(scan.target_dir, relative_file_path)
    expected_file_path = File.join(expectations_dir, relative_expectation_dir, relative_file_path)

    parsed_actual = JSON.parse(File.read(actual_file_path))
    parsed_expected = JSON.parse(File.read(expected_file_path))

    sanitized_actual = sanitize_function.call(parsed_actual)
    sanitized_expected = sanitize_function.call(parsed_expected)

    begin
      expect(sanitized_actual).to include_json(sanitized_expected)
    rescue RSpec::Expectations::ExpectationNotMetError => e
      e.message.prepend("Contents of actual file '#{actual_file_path}' does not match expected file '#{expected_file_path}'\n")
      raise
    end
  end
end
