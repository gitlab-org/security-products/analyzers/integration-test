module GitlabSecure
  module IntegrationTest
    # This class is responsible for copying fixtures directory to a temporary location,
    # and removing old temporary fixture directories
    class Fixtures
      # the maximum number of `test-*` directories to keep in the tmp directory.
      MAX_TMP_SUBIDRS = 10

      # target_dir is the path to the target to be analyzed, after we've copied it to a temporary location
      attr_reader :target_dir

      # fixtures_dir is the top-level directory that contains the fixture files.
      # Example `qa/fixtures`
      #
      # original_target_dir is the original path to the target project to be analyzed, including the enclosing fixtures path.
      # Example: `qa/fixtures/js-npm/default`
      #
      # output_directory is the rspec example path
      # Example: `running-image/with-test-project/with-js-npm/with-lockfile-v1/created-report/behaves-like-non-empty-report/version`
      def initialize(fixtures_dir, original_target_dir, output_directory)
        @original_target_dir = original_target_dir

        # project_dir is the directory of the project to be scanned.
        # Example: `js-npm/default`
        relative_project_dir = original_target_dir.delete_prefix(fixtures_dir)

        # the temporary directory is located where the specs are executed
        @tmp_dir = File.join(FileUtils.pwd, "tmp")

        # the unique_temporary_dir is a unique directory created each time a test is run.
        # Example: `test-62456`
        unique_temporary_dir = "test-#{RSpec.configuration.seed}"

        # the directory that we copy the fixtures to before running the test. This target_dir value includes the rspec example path:
        # Example: `tmp/test-62456/js-npm/default/running-image/with-test-project/with-js-npm/with-lockfile-v1/created-report/behaves-like-non-empty-report/version`
        @target_dir = File.join(@tmp_dir, unique_temporary_dir, relative_project_dir, output_directory)
      end

      def prepare!
        copy_fixtures_dir
        remove_old_temp_qa_dirs
      end

      private

      # Remove the oldest `test-*` directories from the temp directory.
      # This is skipped if there are less than MAX_TMP_SUBIDRS `qa-*` directories.
      def remove_old_temp_qa_dirs
        tmp_fixture_dirs = Dir[File.join(@tmp_dir, "test-*")]
        return if tmp_fixture_dirs.length <= MAX_TMP_SUBIDRS

        oldest_dirs = tmp_fixture_dirs.min_by(tmp_fixture_dirs.length - MAX_TMP_SUBIDRS) { |f| File.mtime(f) }
        oldest_dirs.each { |dir| FileUtils.rm_rf(dir) }
      end

      # Copy the original fixtures dir to the new temporary fixtures location.
      #
      # Example:
      #
      # Layout of project before executing this method:
      #
      # .
      # └── gemnasium/
      #     └── qa/
      #         ├── scripts
      #         ├── expect
      #         └── fixtures/
      #             └── go-modules/
      #                 └── default/
      #                     ├── go.mod
      #                     └── go.sum
      #
      # Layout of project _after_ executing this method. The original `qa/fixtures/go-modules/default` dir is copied to a new
      # `tmp/test-63192/go-modules/default/<rspec-example-name>` directory:
      #
      # .
      # └── gemnasium/
      #     └── tmp/
      #         └── test-63192/
      #             └── go-modules/
      #                 └── default/
      #                     ├── running-image-with-test-project-with-go-modules-behaves-like-successful-scan-creates-a-report/
      #                     │   ├── go.mod
      #                     │   ├── go.sum
      #                     │   ├── gl-sbom-go-go.cdx.json
      #                     │   ├── sbom-manifest.json
      #                     │   └── gl-dependency-scanning-report.json
      #                     └── running-image-with-test-project-with-go-modules-when-excluding-go.sum-with-ds_excluded_paths-behaves-like-successful-scan-creates-a-report/
      #                         ├── go.mod
      #                         ├── go.sum
      #                         ├── gl-sbom-go-go.cdx.json
      #                         ├── sbom-manifest.json
      #                         └── gl-dependency-scanning-report.json
      #
      def copy_fixtures_dir
        FileUtils.mkdir_p(@target_dir)
        # copy the contents of the original target project to the temporary fixtures location pointed to by target_dir
        `cp -LR #{@original_target_dir}/. #{@target_dir}`
      end
    end
  end
end
