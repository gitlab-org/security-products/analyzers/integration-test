require 'json'
require 'English'
require 'pathname'
require 'fileutils'
require_relative 'fixtures'

# DockerRunner is used to scan a target directory using the Docker image of the analyzer,
# and to collect the generated report as well as the analyzer exit code.
# It uses the docker CLI.
#
module GitlabSecure
  module IntegrationTest
    class DockerRunner
      SCRIPT_FILENAME = "run.sh".freeze

      # Result combines the output and exit code of the analyzer
      # with the path of the generated report,
      # and the JSON-decoded report if it exists.
      #
      Result = Struct.new(:report_path, :target_dir, :report, :exit_code, :combined_output)

      # Initialize cache
      @cache = {}

      # Run the analyzer image and scan a target directory
      # or return the result that's already in cache for the same arguments.
      #
      def self.run_with_cache(image_name, fixtures_dir, target_dir, output_dir, **kwargs)
        key = kwargs.merge(image_name: image_name, target_dir: target_dir).to_s

        # return if already in cache
        return @cache[key] if @cache.key?(key)

        # scan target directory, update cache, and return
        @cache[key] = new(image_name, fixtures_dir, target_dir, output_dir, **kwargs).run
      end

      # Run the analyzer image and return the result of the scan.
      #
      # Reports created by previous executions of the analyzer are removed before the scan.
      #
      def run
        fixtures.prepare!
        docker_run
      end

      private

      attr_reader :image_name, :fixtures, :variables, :mount_points, :report_filename, :command,
                  :script, :offline, :privileged, :user, :debug

      # Initialize a runner that scans a target directory with the given analyzer image.
      #
      # variables is a hash that defines environment variables
      # to be set when running the Docker container.
      #
      # command is an optional array that overrides the CMD defined in the Dockerfile.
      #
      # script is an optional Shell script to be executed in the container.
      # If defined, then this script is saved to the target directory
      # and executed in place of the default command.
      #
      # When offline is set the container runs without network connection.
      #
      def initialize(
        image_name, fixtures_dir, target_dir, output_dir,
        variables: {}, mount_points: {}, report_filename: "gl-dependency-scanning-report.json",
        command: [], script: nil, offline: false, privileged: false, user: nil, debug: false)

        @image_name = image_name
        @fixtures = Fixtures.new(fixtures_dir, target_dir, output_dir)
        @variables = variables
        @mount_points = mount_points
        @report_filename = report_filename
        @command = command
        @script = script
        @offline = offline
        @privileged = privileged
        @user = @privileged ? 'root' : user
        @debug = debug
      end

      def report_path
        File.join(fixtures.target_dir, report_filename)
      end

      def report
        JSON.parse(File.read(report_path)) if File.exist?(report_path)
      end

      def has_script?
        !script.nil?
      end

      def script_path
        File.join(fixtures.target_dir, SCRIPT_FILENAME)
      end

      def cmd_and_args
        # execute the Shell script if defined
        return File.join(".", SCRIPT_FILENAME) if has_script?

        # else execute given CMD and ARGS if defined
        command.join(" ")
      end

      def docker_run
        # save Shell script to target  dir if defined
        if has_script?
          File.open(script_path, File::CREAT | File::TRUNC | File::WRONLY, 0755) do |f|
            f.write(script)
          end
        end

        # run docker container, execute given command or Shell script
        mount_dir = "/app"
        opts = %w[-t --rm]
        mount_points.each do |local_dir, docker_dir|
          docker_dir_inside_mount_dir = docker_dir.gsub("$WORKDIR", mount_dir)
          opts << add_mount(File.join(FileUtils.pwd, local_dir.to_s), docker_dir_inside_mount_dir)
        end

        opts << add_mount(fixtures.target_dir, mount_dir)
        opts << ["-w", mount_dir]
        opts << %w[--network none] if offline
        opts << "--user #{user}" if user
        variables.each { |k, v| opts << ["--env", %|#{k}="#{v}"|] }
        combined_output = execute_docker_cmd(opts, image_name, cmd_and_args)
        
        debug_log(message: combined_output, description: "Docker run log")

        Result.new(report_path, fixtures.target_dir, report, $CHILD_STATUS.to_i, combined_output)
      end

      def magenta(str)
        "\e[35m#{str}\e[0m"
      end

      def debug_log(message:, description: nil)
        return unless debug

        if description
          puts magenta("\nXXXXXXXXXXXXXXXX BEGIN #{description} XXXXXXXXXXXXXXXX")
          puts magenta(message)
          puts magenta("XXXXXXXXXXXXXXXX END #{description} XXXXXXXXXXXXXXXX\n")
        else
          puts magenta(message)
        end
      end

      def execute_docker_cmd(opts, image_name, cmd_and_args)
        docker_command = "docker run #{opts.join(' ')} #{image_name} #{cmd_and_args}"
        debug_log(message: "Docker command to execute: #{docker_command}")

        `#{docker_command}`
      end

      def add_mount(dind_host_path, analyzer_path)
        # Set permissive permissions so that non-root users of the analyzers are able to create/modify files in the mount
        # Note: for backward compatibility it doesn't change the permissions if user isn't set
        # Note: there's no need to change the permission if user is root
        `chmod -R 777 #{dind_host_path}` if user&. != 'root'
        return %w[-v] + ["#{dind_host_path}:#{analyzer_path}"]
      end
    end
  end
end
