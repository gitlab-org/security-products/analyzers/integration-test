# frozen_string_literal: true

require 'down'
require 'json'
require 'net/http'
require 'open-uri'

module GitlabSecure
  module IntegrationTest
    # Utilities for GraphQL calls to perform testing
    class GraphqlUtils
      @base_url = 'https://gitlab.com'
      @ssl = true

      def self.ds_artifacts_query
        path = ENV.fetch('CI_PROJECT_PATH')
        iid = ENV.fetch('CI_PIPELINE_IID')

        # TODO: Use GraphQL query variables instead of string interpolation.
        "{project(fullPath: \"#{path}\")" \
                  "{pipeline(iid: #{iid}) {jobs(securityReportTypes: [DEPENDENCY_SCANNING])" \
                  '{nodes {name artifacts {nodes {downloadPath fileType}}}}}}}'
      end

      def self.graphql_call(string)
        uri = URI("#{@base_url}/api/graphql")
        res = Net::HTTP.start(uri.host, uri.port, use_ssl: @ssl) do |http|
          req = Net::HTTP::Post.new(uri)
          req['Content-Type'] = 'application/json'
          req['User-Agent'] = 'GitLab Secure Integration Test GraphQL API Agent'
          req['Job-Token'] = ENV.fetch('CI_JOB_TOKEN')
          # The body needs to be a JSON string.
          req.body = JSON[{ 'query' => string }]

          res = http.request(req)

          puts '========================================================================='
          puts 'GitLab GraphQL API Request'
          puts '========================================================================='
          puts "#{req.method} #{req.uri}"
          req.each_header do |k, v|
            if k == 'job-token'
              puts "#{k}: [redacted]"
            else
              puts "#{k}: #{v}"
            end
          end
          puts
          puts req.body
          puts
          puts '========================================================================='
          puts
          puts

          res
        end

        puts '========================================================================='
        puts 'GitLab GraphQL API Response'
        puts '========================================================================='
        puts "HTTP/#{res.http_version} #{res.code}"
        res.each_header { |k, v| puts "#{k}: #{v}" }
        puts
        puts res.body
        puts
        puts '========================================================================='
        puts
        puts

        res.body
      end

      def self.download_artifact(file_type: 'METADATA', job_name: nil)
        res_json = JSON(graphql_call(ds_artifacts_query))

        pipeline_jobs = res_json.dig('data', 'project', 'pipeline', 'jobs', 'nodes')
        raise('Failed to find jobs for pipeline') if pipeline_jobs.nil? || pipeline_jobs.empty?

        artifact_nodes = []
        pipeline_jobs.each do |pipeline_job|
          next if job_name && pipeline_job['name'] != job_name

          artifact_nodes << pipeline_job.dig('artifacts', 'nodes')
        end
        artifact_nodes.flatten!

        if artifact_nodes.nil? || artifact_nodes.empty?
          raise("Failed to find artifacts for job with name '#{job_name}'") if job_name

          raise('Failed to find artifacts for jobs')
        end

        artifact_nodes.each do |artifact|
          next unless artifact['fileType'] == file_type

          return Down.download(@base_url + artifact['downloadPath'])
        end

        raise("Failed to download artifact with fileType '#{file_type}' and job name '#{job_name}'") if job_name

        raise("Failed to download artifact with fileType '#{file_type}'")
      end

      def self.filesizes_from_match_filenames(metadata_gz, filenames)
        metafile = Zlib::GzipReader.open(metadata_gz)
        metadata = metafile.read

        return_file_sizes = []

        metadata.each_line do |line|
          return_file_sizes << line.match(/(?:size":)(\d*)/)[1] if line.match(filenames)
        end

        return_file_sizes
      end
    end
  end
end
