require_relative './schema_path_resolver'

# returns the path to the CycloneDX schema that corresponds to a given CycloneDX report.
module GitlabSecure
  module IntegrationTest
    module SchemaPathResolver
      class CycloneDX < SecurityReport
        def initialize(report)
          super
          @base_dir = ENV.fetch("CYCLONEDX_REPORT_SCHEMAS_DIR", "cyclonedx-schemas")
        end

        def path
          File.join(base_dir, filename)
        end

        private

        def filename
          "bom-#{report['specVersion']}.schema.json"
        end
      end
    end
  end
end
