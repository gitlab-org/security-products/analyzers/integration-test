# This module monkeypatches the RSpec::JsonExpectations gem (https://github.com/waterlink/rspec-json_expectations)
# so that it returns true when matching fields with the string ":SKIP:" in them. This is useful for ignoring
# fields whose contents are variable, such as timestamps, or version values.

module JsonTraverserExtension
  def self.prepended(base)
    base.singleton_class.send(:prepend, ClassMethods)
  end

  module ClassMethods
    private

    def handle_value(errors, expected, actual, negate=false, prefix=[])
      # this guard condition exists in the original implementation, so we need
      # to call it before our modified matcher in order to maintain the same behaviour
      return nil unless handled_by_simple_value?(expected)

      # This is the behaviour we're changing with this monkeypatch. If a field contains
      # :SKIP: in it, we return true, causing it to match against any input value.
      return true if expected == ':SKIP:'

      super
    end
  end
end

module RSpec::JsonExpectations
  class JsonTraverser
    prepend JsonTraverserExtension
  end
end
