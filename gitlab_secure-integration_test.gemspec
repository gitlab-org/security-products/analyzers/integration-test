# frozen_string_literal: true

require_relative "lib/gitlab_secure/integration_test/version"

Gem::Specification.new do |spec|
  spec.name          = "gitlab_secure-integration_test"
  spec.version       = GitlabSecure::IntegrationTest::VERSION
  spec.authors       = ["Fabien Catteau"]
  spec.email         = ["fcatteau@gitlab.com"]

  spec.summary       = "RSpec shared examples and helpers to test GitLab Secure analyzers"
  spec.homepage      = "https://gitlab.com/gitlab-org/security-products/analyzers/integration-test"
  spec.license       = "MIT"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.require_paths = ["lib"]

  spec.required_ruby_version = Gem::Requirement.new(">= 3.0.5")

  spec.add_runtime_dependency "rspec", "~> 3.10.0"
  spec.add_runtime_dependency "json", "~> 2.6.1"
  spec.add_runtime_dependency "json_schemer", "~> 0.2.18"
  spec.add_runtime_dependency "rspec-json_expectations", "~> 2.2.0"
  spec.add_runtime_dependency "rubyzip", "~> 2.3.2"
  spec.add_runtime_dependency "down", "~> 5.0"

  spec.add_development_dependency "rake", "~> 13.0"
end
