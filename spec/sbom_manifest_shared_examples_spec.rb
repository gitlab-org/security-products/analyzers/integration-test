require 'rspec'
require 'json'

require_relative '../lib/gitlab_secure/integration_test/shared_examples/sbom_manifest_shared_examples'

RSpec.describe "SBOM Manifest shared examples" do
  let(:scan) {
    double(:scan, target_dir: File.expand_path("fixtures/multi-project/subdirs/", __dir__))
  }

  let(:expectations_dir) { File.expand_path("expect", __dir__) }
  let(:relative_sbom_manifest_path) { "sbom-manifest.json" }
  let(:relative_expectation_dir) { "multi-project/subdirs" }

  it_behaves_like "non-empty SBOM manifest"
  it_behaves_like "recorded SBOM manifest"
end
