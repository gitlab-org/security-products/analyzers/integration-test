require 'rspec'
require 'json'

require_relative '../lib/gitlab_secure/integration_test/shared_examples/scan_shared_examples'

RSpec.describe "scan shared examples" do
  Scan = Struct.new(:combined_output, :exit_code, :report_path, :report)

  let(:combined_output) { "" }
  let(:exit_code) { 0 }
  let(:report_path) { nil }

  let(:scan) do
    Scan.new(combined_output, exit_code, report_path)
  end

  context "with successful scan" do
    let(:combined_output) { "[INFO] Some analyzer v1.2.3" }
    let(:exit_code) { 0 }
    let(:report_path) do
      File.expand_path("fixtures/go-modules/gl-sast-report.json", __dir__)
    end

    it_behaves_like "successful scan"
  end

  context "with crashed scan" do
    let(:combined_output) { "[INFO] Some analyzer v1.2.3" }
    let(:exit_code) { 128 }
    let(:report_path) { "missing" }

    it_behaves_like "crashed scan"
  end

  context "with failed scan" do
    let(:combined_output) { "[ERRO] something went wrong" }
    let(:exit_code) { 1 }
    let(:report_path) { "missing" }

    it_behaves_like "failed scan"
  end
end
