# frozen_string_literal: true

require 'rspec'
require 'json'
require 'uri'

require_relative '../lib/gitlab_secure/integration_test/docker_runner'

RSpec.describe GitlabSecure::IntegrationTest::DockerRunner do
  let(:kwargs) { {} }
  # use a different target_dir value for each example, in order to bust the cache
  let(:target_dir) { "target_dir-#{Process.clock_gettime(Process::CLOCK_REALTIME, :nanosecond)}" }
  let(:args) { ['image_name', 'fixtures_dir', target_dir, 'description'] }
  let(:docker_runner) { described_class.new(*args, **kwargs) }
  let(:fixtures_double) do
    instance_double(GitlabSecure::IntegrationTest::Fixtures, :fixture, { 'prepare!': true, target_dir: target_dir })
  end

  subject { described_class.run_with_cache(*args, **kwargs) }

  before do
    allow(GitlabSecure::IntegrationTest::Fixtures).to receive(:new).and_return(fixtures_double)
    allow(FileUtils).to receive(:pwd).and_return('/pwd')
    allow(docker_runner).to receive(:run).and_call_original
    allow(described_class).to receive(:new).and_return(docker_runner)
    allow(docker_runner).to receive(:execute_docker_cmd)
  end

  describe '.run_with_cache' do
    context 'when no arguments are provided' do
      it 'executes the docker command with the default options' do
        expected_opts = ['-t', '--rm', ['-v', "#{target_dir}:/app"], ['-w', '/app']]
        expect(docker_runner).to receive(:execute_docker_cmd).with(expected_opts, 'image_name', '')
        subject
      end
    end

    context 'when the user argument is provided' do
      let(:default_directory_permissions) { 0o777 - File.umask }

      let(:kwargs) do
        { user: 'some_user' }
      end

      around do |example|
        FileUtils.mkdir_p(target_dir)
        example.run
        FileUtils.rm_rf(target_dir)
      end

      context 'and the user name is not root' do
        it 'passes the --user flag to the Docker run command' do
          expected_opts = ['-t', '--rm', ['-v', "#{target_dir}:/app"], ['-w', '/app'], '--user some_user']
          expect(docker_runner).to receive(:execute_docker_cmd).with(expected_opts, 'image_name', '')
          subject
        end

        it 'changes the permissions of the target dir' do
          expect { subject }.to change { File.stat(target_dir).mode & 0o777 }
            .from(default_directory_permissions).to(0o777)
        end
      end

      context 'and the user name is root' do
        let(:kwargs) do
          { user: 'root' }
        end

        it 'does not change the permissions of the target dir' do
          expect { subject }.not_to change { File.stat(target_dir).mode & 0o777 }.from(default_directory_permissions)
        end
      end
    end

    context 'when the mount_points argument is provided' do
      let(:kwargs) do
        { mount_points: { 'path/to/local/dir1': 'path/to/docker/dir1', 'path/to/local/dir2': 'path/to/docker/dir2' } }
      end

      it 'executes the docker command with additional mount points' do
        expected_opts = ['-t', '--rm', ['-v', '/pwd/path/to/local/dir1:path/to/docker/dir1'],
                         ['-v', '/pwd/path/to/local/dir2:path/to/docker/dir2'],
                         ['-v', anything], ['-w', '/app']]
        expect(docker_runner).to receive(:execute_docker_cmd).with(expected_opts, 'image_name', '')
        subject
      end

      context 'and one of the mount points contains a $WORKDIR placeholder' do
        let(:kwargs) do
          { mount_points: { 'path/to/local/dir1': '$WORKDIR/path/to/docker/dir1',
                            'path/to/local/dir2': 'path/to/docker/dir2' } }
        end

        it 'executes the docker command with additional mount points' do
          expected_opts = ['-t', '--rm', ['-v', '/pwd/path/to/local/dir1:/app/path/to/docker/dir1'],
                           ['-v', '/pwd/path/to/local/dir2:path/to/docker/dir2'],
                           ['-v', anything], ['-w', '/app']]
          expect(docker_runner).to receive(:execute_docker_cmd).with(expected_opts, 'image_name', '')
          subject
        end
      end
    end
  end
end
