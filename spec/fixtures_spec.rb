require 'rspec'

require 'fileutils'
require_relative '../lib/gitlab_secure/integration_test/fixtures'

RSpec.describe GitlabSecure::IntegrationTest::Fixtures do
  let(:subject) do
    described_class.new("qa/fixtures", "some/target/project", "some-description")
  end

  let(:tmp_dir) { File.join(FileUtils.pwd, "tmp") }

  before do
    FileUtils.mkdir_p(tmp_dir)

    (1..20).each do |i|
      FileUtils.mkdir_p(File.join(tmp_dir, "test-#{i}"))
    end
  end

  after do
    FileUtils.rm_rf(tmp_dir)
  end

  it 'deletes the oldest tmp sub directories to ensure we do not exceed the MAX_TMP_SUBIDRS value' do
    expect(subject).to receive(:copy_fixtures_dir).and_return(true)

    subject.prepare!

    expect(Dir["tmp/test-*"].length).to eq(described_class::MAX_TMP_SUBIDRS)
  end
end
