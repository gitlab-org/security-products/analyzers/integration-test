require 'rspec'
require 'json'

require_relative '../lib/gitlab_secure/integration_test/shared_examples/report_shared_examples'

RSpec.describe "report shared examples" do
  def parse(path)
    JSON.parse(File.read(File.expand_path(path, __dir__)))
  end

  let(:report) do
    parse(report_path)
  end

  let(:scan) { double(:scan, report_path: File.join(__dir__, report_path)) }

  context "with Dependency Scanning report" do
    let(:report_path) { "fixtures/go-modules/gl-dependency-scanning-report.json" }

    it_behaves_like "non-empty report", ["go.sum"]
    it_behaves_like "recorded report" do
      let(:recorded_report) { report }
    end

    it_behaves_like "valid report"

    context "when the report contains skipped entries" do
      let(:report_path) { "fixtures/js-yarn/default/gl-dependency-scanning-report.json" }

      it_behaves_like "recorded report" do
        let(:recorded_report) { parse("expect/js-yarn/default/gl-dependency-scanning-report.json") }
      end
    end

    context 'when the report contains a skipped entry that changes the sort order of the vulnerabilities' do
      let(:report) do
        JSON.parse(<<~HEREDOC)
        {
          "vulnerabilities": [
            {
              "description": "description-1",
              "cve": "cve-value-1"
            },
            {
              "description": "another description",
              "cve": "cve-value-2"
            }
          ],
          "scan": {
            "start_time": "start-time"
          }
        }
        HEREDOC
      end

      it_behaves_like "recorded report" do
        let(:recorded_report) do
          JSON.parse(<<~HEREDOC)
          {
            "vulnerabilities": [
              {
                "description": ":SKIP:",
                "cve": "cve-value-1"
              },
              {
                "description": "another description",
                "cve": "cve-value-2"
              }
            ],
            "scan": {
              "start_time": "start-time"
            }
          }
          HEREDOC
        end
      end
    end

    context 'when the reports do not match' do
      context 'and the expected report is a subset of the actual report' do
        let(:report) do
          JSON.parse(<<~HEREDOC)
          {
            "vulnerabilities": [
              { "name": "a" },
              { "name": "b" },
              { "name": "d" },
              { "name": "e" }
            ],
            "scan": { "start_time": "start-time", "end_time": "end-time" }
          }
          HEREDOC
        end

        it_behaves_like "recorded report" do
          let(:recorded_report) do
            JSON.parse(<<~HEREDOC)
            {
              "vulnerabilities": [
                { "name": "a" },
                { "name": "b" },
                { "name": "d" }
              ],
              "scan": { "start_time": "start-time", "end_time": "end-time" }
            }
            HEREDOC
          end
        end
      end

      context 'and the expected report is not a strict subset of the actual report because the elements are ordered differently' do
        it 'outputs the exact JSON path where the two reports differ' do
          reporter = RSpec::Core::Reporter.new(RSpec::Core::Configuration.new)
          RSpec.describe {
            let(:report) do
              JSON.parse(<<~HEREDOC)
              {
                "vulnerabilities": [
                  { "name": "a" },
                  { "name": "b" },
                  { "name": "c" },
                  { "name": "d" },
                  { "name": "e" }
                ],
                "scan": { "start_time": "start-time", "end_time": "end-time" }
              }
              HEREDOC
            end

            # force report_path to a file guaranteed to exist, so assert_report_exists returns true
            let(:scan) { double(:scan, report_path: __FILE__) }

            it_behaves_like "recorded report" do
              let(:recorded_report) do
                JSON.parse(<<~HEREDOC)
                {
                  "vulnerabilities": [
                    { "name": "a" },
                    { "name": "b" },
                    { "name": "d" }
                  ],
                  "scan": { "start_time": "start-time", "end_time": "end-time" }
                }
                HEREDOC
              end
            end
          }.run(reporter)

          expected_error = <<-EOF
          json atom at path "2/name" is not equal to expected value:

            expected: "d"
                 got: "c"
          EOF

          failed_example = reporter.examples.find do |ex|
            ex.metadata[:full_description] == ' behaves like recorded report vulnerabilities is equivalent'
          end

          expect(failed_example.execution_result.exception.class).to eql(RSpec::Expectations::ExpectationNotMetError)
          expect(failed_example.execution_result.exception.message).to match(expected_error)
        end
      end
    end

    context "when the report is invalid" do
      it "outputs the reason why the report failed JSON validation" do
        reporter = RSpec::Core::Reporter.new(RSpec::Core::Configuration.new)
        RSpec.describe {
          let(:report_path) { "fixtures/go-modules/invalid-report/gl-dependency-scanning-report.json" }

          let(:report) do
            JSON.parse(File.read(File.expand_path(report_path, __dir__)))
          end

          let(:scan) { double(:scan, report_path: File.join(__dir__, report_path)) }

          it_behaves_like "valid report"
        }.run(reporter)

        expected_error = <<~EOF
       JSON validation against schema '.*security-report-schemas/v14.0.4/dependency-scanning-report-format.json' failed with the following errors:

         property '/scan/analyzer/id' is invalid: error_type=minLength
         property '/scan/analyzer/name' is invalid: error_type=minLength
         property '/scan/analyzer/vendor/name' is invalid: error_type=minLength
         property '/scan/analyzer/version' is invalid: error_type=minLength

         EOF

        # remove trailing newline from expected_error heredoc
        expected_error.strip!

        expect(reporter.examples.first.execution_result.exception.class).to eql(RSpec::Expectations::ExpectationNotMetError)
        expect(reporter.examples.first.execution_result.exception.message).to match(expected_error)
      end
    end
  end

  context 'when the actual report does not exist' do
    it "outputs an error message explaining that the report does not exist" do
      reporter = RSpec::Core::Reporter.new(RSpec::Core::Configuration.new)
      RSpec.describe {
        let(:report_path) { "non/existent/path/gl-dependency-scanning-report.json" }

        let(:scan) { double(:scan, report_path: File.join(__dir__, report_path)) }

        it_behaves_like "valid report"
      }.run(reporter)

      expected_error = "Expected report '.*/spec/non/existent/path/gl-dependency-scanning-report.json' to exist"

      expect(reporter.examples.first.execution_result.exception.class).to eql(RSpec::Expectations::ExpectationNotMetError)
      expect(reporter.examples.first.execution_result.exception.message).to match(expected_error)
    end
  end

  context "with empty Dependency Scanning report" do
    let(:report_path) { "fixtures/no-vulnerabilities/gl-dependency-scanning-report.json" }

    it_behaves_like "empty report"
    it_behaves_like "recorded report" do
      let(:recorded_report) { report }
    end

    it_behaves_like "valid report"
  end

  context "with SAST report" do
    let(:report_path) { "fixtures/go-modules/gl-sast-report.json" }

    it_behaves_like "non-empty report", ["main.go"]
    it_behaves_like "recorded report" do
      let(:recorded_report) { report }
    end

    it_behaves_like "valid report"
  end

  context "with Secret Detection report" do
    let(:report_path) { "fixtures/secrets/gl-secret-detection-report.json" }

    it_behaves_like "non-empty report", ["id_rsa", "keys/aws", "keys/dsa_key", "keys/ecdsa_key", "keys/ed25519_key", "keys/pgp_ecc_key", "keys/pgp_rsa_key", "keys/rsa_key", "keys/slack_key", "keys/stripe_key", "keys/testkeys-110044-23ecb974342473e425f395ddd5f8f787ac6be90d.json", "keys/twilio_key", "main.php"]
    it_behaves_like "recorded report" do
      let(:recorded_report) { report }
    end

    it_behaves_like "valid report"
  end
end
