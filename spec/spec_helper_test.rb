require 'rspec'

require_relative '../lib/gitlab_secure/integration_test/spec_helper'

RSpec.describe GitlabSecure::IntegrationTest do
  describe "some description" do
    context "some context" do
      it "copies the rspec example name into the description instance variable" do
        expect(@description).to eql("GitlabSecure::IntegrationTest some description some context copies the rspec example name into the description instance variable")
      end
    end
  end
end
