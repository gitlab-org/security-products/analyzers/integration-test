# frozen_string_literal: true

require 'rspec'
require 'gitlab_secure/integration_test/graphql_utils'

RSpec.describe GitlabSecure::IntegrationTest::GraphqlUtils do
  describe '.ds_artifacts_query' do
    context 'gets CI variables' do
      before do
        ENV['CI_PROJECT_PATH'] = '/foo/bar'
        ENV['CI_PIPELINE_IID'] = '42'
      end

      it 'returns a query string' do
        expect(GitlabSecure::IntegrationTest::GraphqlUtils.ds_artifacts_query).to eql('{project(fullPath: "/foo/bar")' \
                  '{pipeline(iid: 42) {jobs(securityReportTypes: [DEPENDENCY_SCANNING])' \
                  '{nodes {name artifacts {nodes {downloadPath fileType}}}}}}}')
      end
    end
  end

  describe '.graphql_call' do
    context 'creates an HTTP POST' do
      before do
        ENV['CI_JOB_TOKEN'] = 'token_string_here123'
      end

      it 'returns result body' do
        http_post_dbl = instance_double(Net::HTTP::Post)
        res_dbl = instance_double(Net::HTTPResponse)

        allow_any_instance_of(Net::HTTP).to receive(:request).and_return(res_dbl)

        allow(res_dbl).to receive(:http_version).and_return('1.1')
        allow(res_dbl).to receive(:code).and_return('200')
        allow(res_dbl).to receive(:each_header)
          .and_yield('accept', '*/*')
          .and_yield('user-agent', 'GitLab Secure Integration Test GraphQL API Agent')
        allow(res_dbl).to receive(:body).and_return('fooresponse').thrice
        allow(Net::HTTP::Post).to receive(:new).and_return(http_post_dbl)
        allow(http_post_dbl).to receive(:[]=).with('Content-Type', 'application/json')
        allow(http_post_dbl).to receive(:[]=).with('Job-Token', 'token_string_here123')
        allow(http_post_dbl).to receive(:[]=).with('User-Agent', 'GitLab Secure Integration Test GraphQL API Agent')
        allow(http_post_dbl).to receive(:uri).and_return('https://www.example.gitlab.com')
        allow(http_post_dbl).to receive(:method).and_return('POST')
        allow(http_post_dbl).to receive(:each_header)
          .and_yield('accept', '*/*')
          .and_yield('user-agent', 'GitLab Secure Integration Test GraphQL API Agent')
        allow(http_post_dbl).to receive(:body)

        expect(http_post_dbl).to receive(:body=).with(JSON[{ 'query' => 'foorequest' }])
        expect(GitlabSecure::IntegrationTest::GraphqlUtils.graphql_call('foorequest')).to eql('fooresponse')
      end
    end
  end

  describe '.download_artifact' do
    let(:graphql_response) do
      <<~GRAPHQL_RESPONSE
        {
          "data": {
            "project": {
              "pipeline": {
                "jobs": {
                  "nodes": [
                    {
                      "name": "checkout-by-commit-sha",
                      "artifacts": {
                        "nodes": [
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914281/artifacts/download?file_type=trace",
                            "fileType": "TRACE"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914281/artifacts/download?file_type=dependency_scanning",
                            "fileType": "DEPENDENCY_SCANNING"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914281/artifacts/download?file_type=metadata",
                            "fileType": "METADATA"
                          }
                        ]
                      }
                    },
                    {
                      "name": "new-tag-checkout",
                      "artifacts": {
                        "nodes": [
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914279/artifacts/download?file_type=trace",
                            "fileType": "TRACE"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914279/artifacts/download?file_type=dependency_scanning",
                            "fileType": "DEPENDENCY_SCANNING"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914279/artifacts/download?file_type=metadata",
                            "fileType": "METADATA"
                          }
                        ]
                      }
                    },
                    {
                      "name": "old-tag-checkout",
                      "artifacts": {
                        "nodes": [
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914278/artifacts/download?file_type=trace",
                            "fileType": "TRACE"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914278/artifacts/download?file_type=dependency_scanning",
                            "fileType": "DEPENDENCY_SCANNING"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914278/artifacts/download?file_type=metadata",
                            "fileType": "METADATA"
                          }
                        ]
                      }
                    },
                    {
                      "name": "new-branch-checkout",
                      "artifacts": {
                        "nodes": [
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914276/artifacts/download?file_type=trace",
                            "fileType": "TRACE"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914276/artifacts/download?file_type=dependency_scanning",
                            "fileType": "DEPENDENCY_SCANNING"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914276/artifacts/download?file_type=metadata",
                            "fileType": "METADATA"
                          }
                        ]
                      }
                    },
                    {
                      "name": "old-branch-checkout",
                      "artifacts": {
                        "nodes": [
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914275/artifacts/download?file_type=trace",
                            "fileType": "TRACE"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914275/artifacts/download?file_type=dependency_scanning",
                            "fileType": "DEPENDENCY_SCANNING"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914275/artifacts/download?file_type=metadata",
                            "fileType": "METADATA"
                          }
                        ]
                      }
                    },
                    {
                      "name": "branch-updated",
                      "artifacts": {
                        "nodes": [
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914273/artifacts/download?file_type=trace",
                            "fileType": "TRACE"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914273/artifacts/download?file_type=dependency_scanning",
                            "fileType": "DEPENDENCY_SCANNING"
                          },
                          {
                            "downloadPath": "/gitlab-org/security-products/tests/go-modules/-/jobs/3798914273/artifacts/download?file_type=metadata",
                            "fileType": "METADATA"
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            }
          }
        }
      GRAPHQL_RESPONSE
    end

    before do
      allow(GitlabSecure::IntegrationTest::GraphqlUtils).to receive(:ds_artifacts_query)
      allow(GitlabSecure::IntegrationTest::GraphqlUtils).to receive(:graphql_call).and_return(graphql_response)
    end

    context 'when no jobs exist for the pipeline' do
      let(:graphql_response) do
        <<~GRAPHQL_RESPONSE
          {
            "data": {
              "project": {
                "pipeline": {
                  "jobs": {
                    "nodes": []
                  }
                }
              }
            }
          }
        GRAPHQL_RESPONSE
      end

      it 'raises an error' do
        expect do
          GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact
        end.to raise_error('Failed to find jobs for pipeline')
      end
    end

    context 'when jobs exist for the pipeline' do
      context 'and no job_name parameter is provided' do
        context 'and no artifacts exist for the pipeline jobs' do
          let(:graphql_response) do
            <<~GRAPHQL_RESPONSE
              {
                "data": {
                  "project": {
                    "pipeline": {
                      "jobs": {
                        "nodes": [
                          {
                            "name": "checkout-by-commit-sha",
                            "artifacts": {
                              "nodes": []
                            }
                          },
                          {
                            "name": "new-tag-checkout",
                            "artifacts": {
                              "nodes": []
                            }
                          }
                        ]
                      }
                    }
                  }
                }
              }
            GRAPHQL_RESPONSE
          end

          it 'raises an error' do
            expect do
              GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact
            end.to raise_error('Failed to find artifacts for jobs')
          end
        end

        context 'and no artifacts exist for the pipeline jobs' do
          context 'and no file_type parameter is provided' do
            it 'downloads the artifact for the METADATA file_type from the first job' do
              download_dbl = instance_double(File)

              expect(Down).to receive(:download).with('https://gitlab.com/gitlab-org/security-products/tests/go-modules/-/jobs/3798914281/artifacts/download?file_type=metadata').and_return(download_dbl)
              expect(GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact).to eql(download_dbl)
            end
          end

          context 'and a file_type parameter is provided' do
            it 'downloads the artifact for the given file_type from the first job' do
              download_dbl = instance_double(File)

              expect(Down).to receive(:download).with('https://gitlab.com/gitlab-org/security-products/tests/go-modules/-/jobs/3798914281/artifacts/download?file_type=dependency_scanning').and_return(download_dbl)
              expect(GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact(file_type: 'DEPENDENCY_SCANNING')).to eql(download_dbl)
            end

            context 'and the file_type is not found' do
              it 'raises an error' do
                expect do
                  GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact(file_type: 'foo')
                end.to raise_error("Failed to download artifact with fileType 'foo'")
              end
            end
          end
        end
      end

      context 'and a job_name parameter is provided' do
        context 'and no artifacts exist for the pipeline jobs' do
          let(:graphql_response) do
            <<~GRAPHQL_RESPONSE
              {
                "data": {
                  "project": {
                    "pipeline": {
                      "jobs": {
                        "nodes": [
                          {
                            "name": "checkout-by-commit-sha",
                            "artifacts": {
                              "nodes": []
                            }
                          },
                          {
                            "name": "new-tag-checkout",
                            "artifacts": {
                              "nodes": []
                            }
                          }
                        ]
                      }
                    }
                  }
                }
              }
            GRAPHQL_RESPONSE
          end

          it 'raises an error' do
            expect do
              GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact(job_name: 'new-tag-checkout')
            end.to raise_error("Failed to find artifacts for job with name 'new-tag-checkout'")
          end
        end

        context 'and no file_type parameter is provided' do
          it 'downloads the artifact for the METADATA file_type from the given job' do
            download_dbl = instance_double(File)

            expect(Down).to receive(:download).with('https://gitlab.com/gitlab-org/security-products/tests/go-modules/-/jobs/3798914278/artifacts/download?file_type=metadata').and_return(download_dbl)
            expect(GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact(job_name: 'old-tag-checkout')).to eql(download_dbl)
          end
        end

        context 'and a file_type parameter is provided' do
          it 'downloads the artifact for the given file_type from the given job' do
            download_dbl = instance_double(File)

            expect(Down).to receive(:download).with('https://gitlab.com/gitlab-org/security-products/tests/go-modules/-/jobs/3798914276/artifacts/download?file_type=dependency_scanning').and_return(download_dbl)
            expect(GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact(file_type: 'DEPENDENCY_SCANNING', job_name: 'new-branch-checkout')).to eql(download_dbl)
          end

          context 'and the file_type is not found' do
            it 'raises an error' do
              expect do
                GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact(file_type: 'foo', job_name: 'new-branch-checkout')
              end.to raise_error("Failed to download artifact with fileType 'foo' and job name 'new-branch-checkout'")
            end
          end

          context 'and the job_name is not found' do
            it 'raises an error' do
              expect do
                GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact(file_type: 'foo', job_name: 'unknown-job')
              end.to raise_error("Failed to find artifacts for job with name 'unknown-job'")
            end
          end
        end
      end
    end
  end

  describe '.filesizes_from_match_filenames' do
    let(:metadata) do
      <<~METADATA
        &GitLab Build Artifacts Metadata 0.0.2
        {}api/{}
        api/gradle-dependencies.jsonO{"modified":1646153250,"mode":"644","crc":2705771460,"size":3215,"zipped":516}
        gradle-dependencies.jsonN{"modified":1646153248,"mode":"644","crc":3715801612,"size":363,"zipped":162}
        model/{}
        model/gradle-dependencies.jsonN{"modified":1646153250,"mode":"644","crc":1122631065,"size":791,"zipped":222}
        web/{}
        web/gradle-dependencies.jsonO{"modified":1646153250,"mode":"644","crc":3699273267,"size":1900,"zipped":315}
      METADATA
    end

    context 'takes a zip filename and pattern' do
      it 'returns filenames if a match is found' do
        input_zip_file = instance_double(File)
        metadata_file_double = instance_double(Zlib::GzipReader)

        allow(Zlib::GzipReader).to receive(:open).with(input_zip_file).and_return(metadata_file_double)
        allow(metadata_file_double).to receive(:read).and_return(metadata)

        expect(
          GitlabSecure::IntegrationTest::GraphqlUtils.filesizes_from_match_filenames(
            input_zip_file, /gradle-dependencies.json/
          )
        ).to eql(%w[3215 363 791 1900])
      end

      it 'returns empty array if a match is not found' do
        input_zip_file = instance_double(File)
        metadata_file_double = instance_double(Zlib::GzipReader)

        allow(Zlib::GzipReader).to receive(:open).with(input_zip_file).and_return(metadata_file_double)
        allow(metadata_file_double).to receive(:read).and_return(metadata)

        expect(
          GitlabSecure::IntegrationTest::GraphqlUtils.filesizes_from_match_filenames(input_zip_file, /nomatch/)
        ).to eql([])
      end
    end
  end
end
