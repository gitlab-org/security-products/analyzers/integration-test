require 'rspec'

RSpec.shared_examples_for "successful rspec run" do
  it "reports no failures and no errors" do
    expect(subject).to match(/\d+ example(s)?, 0 failures\n/)
  end
end

RSpec.shared_examples_for "failed rspec run" do
  it "reports failures but no errors" do
    subject
    expect(subject).to match(/\d+ example(s)?, \d+ failures?\n/)
    expect(subject).not_to match(", 0 failures")
  end
end

RSpec.shared_examples_for "error rspec run" do
  it "reports errors" do
    expect(subject).to match(/\d+ example(s)?, \d+ failures?, \d+ error(s)? occurred outside of examples\n/)
    expect(subject).not_to match("failures\n")
  end
end

RSpec.describe "QA script" do
  def fixtures_path(relative)
    File.join(File.expand_path("../spec/fixtures", __dir__), relative)
  end

  def fixtures_url(relative)
    project_url = ENV.fetch("CI_PROJECT_URL", "https://gitlab.com/gitlab-org/security-products/analyzers/integration-test")
    File.join(project_url, "/-/raw/main/spec/fixtures", relative)
  end

  subject { `#{File.expand_path("../scripts/" + script, __dir__)}` }

  describe "dependency-scanning-qa.rb" do
    let(:script) { "dependency-scanning-qa.rb" }

    context "with no path to actual report" do
      before do
        ENV['DS_REPORT_PATH'] = '/foo/bar'
      end

      after do
        ENV.delete('DS_REPORT_PATH')
      end

      specify { expect(subject).to match("cannot load actual report") }
    end

    context "with actual report path" do
      around(:each) do |example|
        ENV["DS_REPORT_PATH"] = fixtures_path("go-modules/gl-dependency-scanning-report.json")
        example.run
        ENV.delete("DS_REPORT_PATH")
      end

      context  "with no expected report URL" do
        before do
          ENV.delete("DS_REPORT_URL")
        end

        specify { expect(subject).to match("cannot load expected report") }
      end

      context "with expected report URL" do
        context "when reports are the same" do
          around(:each) do |example|
            ENV["DS_REPORT_URL"] = fixtures_url("go-modules/gl-dependency-scanning-report.json")
            example.run
            ENV.delete("DS_REPORT_URL")
          end

          it_behaves_like "successful rspec run"
        end

        context "when reports are different" do
          around(:each) do |example|
            ENV["DS_REPORT_URL"] = fixtures_url("python-pip/gl-dependency-scanning-report.json")
            example.run
            ENV.delete("DS_REPORT_URL")
          end

          it_behaves_like "failed rspec run"
        end

        context 'with report empty file' do
          before do
            ENV['DS_REPORT_URL'] = fixtures_path('graphs/empty.gz')
          end

          it_behaves_like 'error rspec run'
        end
      end
    end
  end

  describe "sast-qa.rb" do
    let(:script) { "sast-qa.rb" }

    context "with no path to actual report" do
      before do
        ENV.delete("SAST_REPORT_PATH")
      end

      specify { expect(subject).to match("cannot load actual report") }
    end

    context "with actual report path" do
      around(:each) do |example|
        ENV["SAST_REPORT_PATH"] = fixtures_path("go-modules/gl-sast-report.json")
        example.run
        ENV.delete("SAST_REPORT_PATH")
      end

      context  "with no expected report URL" do
        before do
          ENV.delete("SAST_REPORT_URL")
        end

        specify { expect(subject).to match("cannot load expected report") }
      end

      context "with expected report URL" do
        context "when reports are the same" do
          around(:each) do |example|
            ENV["SAST_REPORT_URL"] = fixtures_url("go-modules/gl-sast-report.json")
            example.run
            ENV.delete("SAST_REPORT_URL")
          end

          it_behaves_like "successful rspec run"
        end

        context "when reports are different" do
          around(:each) do |example|
            ENV["SAST_REPORT_URL"] = fixtures_url("python-pip/gl-sast-report.json")
            example.run
            ENV.delete("SAST_REPORT_URL")
          end

          it_behaves_like "failed rspec run"
        end
      end
    end
  end

  describe "secret-detection-qa.rb" do
    let(:script) { "secret-detection-qa.rb" }

    context "with no path to actual report" do
      before do
        ENV.delete("SECRET_DETECTION_REPORT_PATH")
      end

      specify { expect(subject).to match("cannot load actual report") }
    end

    context "with actual report path" do
      around(:each) do |example|
        ENV["SECRET_DETECTION_REPORT_PATH"] = fixtures_path("secrets/gl-secret-detection-report.json")
        example.run
        ENV.delete("SECRET_DETECTION_REPORT_PATH")
      end

      context  "with no expected report URL" do
        before do
          ENV.delete("SECRET_DETECTION_REPORT_URL")
        end

        specify { expect(subject).to match("cannot load expected report") }
      end

      context "with expected report URL" do
        context "when reports are the same" do
          around(:each) do |example|
            ENV["SECRET_DETECTION_REPORT_URL"] = fixtures_url("secrets/gl-secret-detection-report.json")
            example.run
            ENV.delete("SECRET_DETECTION_REPORT_URL")
          end

          it_behaves_like "successful rspec run"
        end

        context "when reports are different" do
          around(:each) do |example|
            ENV["SECRET_DETECTION_REPORT_URL"] = fixtures_url("secrets-commits/gl-secret-detection-report.json")
            example.run
            ENV.delete("SECRET_DETECTION_REPORT_URL")
          end

          it_behaves_like "failed rspec run"
        end
      end
    end
  end

  describe 'test-export-presence.rb' do
    let(:script) { 'test-export-presence.rb' }

    context 'with actual graph filename matching' do
      before do
        ENV['FILENAMES'] = 'pipdeptree.json gemnasium-maven-plugin.json gradle-dependencies.json dependencies-compile.dot'
        ENV['METADATA_PATH'] = fixtures_path('graphs/metadata.gz')
      end

      it_behaves_like 'successful rspec run'
    end

    context 'with incorrect filename match' do
      before do
        ENV['FILENAMES'] = 'foo'
        ENV['METADATA_PATH'] = fixtures_path('graphs/metadata.gz')
      end

      it_behaves_like 'failed rspec run'
    end

    context 'with filenames not specified' do
      before do
        ENV.delete('FILENAMES')
        ENV['METADATA_PATH'] = fixtures_path('graphs/metadata.gz')
      end

      it_behaves_like 'error rspec run'
    end

    context 'with metadata_path empty' do
      before do
        ENV['FILENAMES'] = 'foo'
        ENV['METADATA_PATH'] = ''
      end

      it_behaves_like 'error rspec run'
    end

    context 'with metadata_path empty file' do
      before do
        ENV['FILENAMES'] = 'foo'
        ENV['METADATA_PATH'] = fixtures_path('graphs/empty.gz')
      end

      it_behaves_like 'error rspec run'
    end
  end
end
