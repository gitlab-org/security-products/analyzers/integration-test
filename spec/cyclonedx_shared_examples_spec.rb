require 'rspec'
require 'json'

require_relative '../lib/gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'

RSpec.describe "CycloneDX shared examples" do
  let(:scan) { double(:scan, target_dir: File.expand_path("fixtures/go-modules/", __dir__)) }
  let(:expectations_dir) { File.expand_path("expect", __dir__) }
  let(:relative_sbom_paths) { ["gl-sbom-go-go.cdx.json"] }
  let(:relative_expectation_dir) { "go-modules" }

  context "with valid CycloneDX files" do
    it_behaves_like "non-empty CycloneDX files"
    it_behaves_like "recorded CycloneDX files"

    it_behaves_like "expected CycloneDX metadata tool-name", "Gemnasium"

    context "schema validation" do
      before do
        stub_const('ENV', ENV.to_hash.merge('CYCLONEDX_REPORT_SCHEMAS_DIR' => 'spec/fixtures/minimal-cyclonedx-schemas'))
      end

      it_behaves_like "valid CycloneDX files"
    end
  end
end
