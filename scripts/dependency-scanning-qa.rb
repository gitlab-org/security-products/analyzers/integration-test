#!/usr/bin/env rspec

require 'json'
require 'open-uri'
require 'gitlab_secure/integration_test/graphql_utils'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'

actual_report = begin
  # TODO: is DS_REPORT_PATH used by anything?
  if ENV['DS_REPORT_PATH']
    actual_report_path = File.expand_path(ENV.fetch('DS_REPORT_PATH', 'gl-dependency-scanning-report.json'))

    raise "actual report does not exist: #{actual_report_path}" unless File.file?(actual_report_path)
  else
    actual_report_path = GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact(file_type: 'DEPENDENCY_SCANNING', job_name: ENV['SCANNER_JOB_NAME'])
  end

  file_contents = File.read(actual_report_path)
  raise("DS report with path #{actual_report_path} was empty") if file_contents.empty?

  JSON.parse(file_contents)
rescue StandardError => e
  raise "cannot load actual report path: #{e}"
end

expected_report = begin
  JSON.parse(URI.open(ENV['DS_REPORT_URL']).read)
rescue OpenURI::HTTPError => e
  raise "cannot open expected report with URL #{ENV['DS_REPORT_URL']}: #{e}"
rescue StandardError => e
  raise "cannot load expected report: #{e}"
end

context 'generated report' do
  let(:report) { actual_report }

  let(:scan) { double(:scan, report_path: actual_report_path) }

  it_behaves_like 'recorded report' do
    let(:recorded_report) { expected_report }
  end

  it_behaves_like 'valid report'
end
