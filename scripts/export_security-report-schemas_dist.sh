#!/bin/bash

# Iterate the git tags of the security-report-schemas repository
# and for each tag create a directory named after the tag,
# and that contains the JSON schemas contained in "dist".
#
# For instance, if the target directory (first argument) is "schemas",
# then the SAST report schema v14.0.0 is exported to
# "schemas/v14.0.0/sast-report-schema.json".

set -e

# argument: path to where the "dist" directory is exported
TARGET_DIR="${1:-"./security-report-schemas"}"
echo target directory: $TARGET_DIR

# apply default values if environment variables are not configured
echo git repository: ${SECURITY_REPORT_SCHEMAS_GIT_REPO:="https://gitlab.com/gitlab-org/security-products/security-report-schemas.git"}
echo git clone: ${SECURITY_REPORT_SCHEMAS_GIT_CLONE:="/tmp/security-report-schemas"}

# clone git repository if needed
if ! test -d "$SECURITY_REPORT_SCHEMAS_GIT_CLONE"; then
  echo "cloning git repository"
  git clone "$SECURITY_REPORT_SCHEMAS_GIT_REPO" "$SECURITY_REPORT_SCHEMAS_GIT_CLONE"
else
  echo "git clone already exists"
fi

# list tags starting with "v"
tags=$(git -C "$SECURITY_REPORT_SCHEMAS_GIT_CLONE" tag -l 'v*')

# iterate tags and export "dist" directory
for tag in $tags; do
  tag_dir="$TARGET_DIR/$tag"
  echo "exporting $tag to $tag_dir"
  mkdir -p "$tag_dir"
  git -C "$SECURITY_REPORT_SCHEMAS_GIT_CLONE" archive --format=tar $tag:dist|tar -C "$tag_dir" -x
done
