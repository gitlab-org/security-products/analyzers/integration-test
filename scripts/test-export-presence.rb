#!/usr/bin/env rspec
# frozen_string_literal: true

require 'json'
require 'open-uri'
require 'gitlab_secure/integration_test/graphql_utils'
require 'gitlab_secure/integration_test/shared_examples/graph_shared_examples'

filenames = ENV['FILENAMES'] || raise('No filenames provided')

artifact = ENV['METADATA_PATH'] || GitlabSecure::IntegrationTest::GraphqlUtils.download_artifact
raise('No metadata artifact provided or returned') if artifact.nil? || File.read(artifact).empty?

context 'analyzer' do
  it_behaves_like 'provides an export on failure' do
    let(:filenames) { Regexp.union(filenames.split) }
    let(:artifact) { artifact }
  end
end
