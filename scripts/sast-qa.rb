#!/usr/bin/env rspec

require 'json'
require 'open-uri'

require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'

actual_report_path = ENV.fetch('SAST_REPORT_PATH', 'gl-sast-report.json')

actual_report =
  begin
    JSON.parse(File.read(actual_report_path))
  rescue StandardError => e
    raise "cannot load actual report with path #{actual_report_path}: #{e}"
  end

expected_report =
  begin
    JSON.parse(URI.open(ENV['SAST_REPORT_URL']).read)
  rescue OpenURI::HTTPError => e
    raise "cannot open expected report with URL #{ENV['SAST_REPORT_URL']}: #{e}"
  rescue StandardError => e
    raise "cannot load expected report: #{e}"
  end

context 'generated report' do
  let(:report) { actual_report }

  let(:scan) { double(:scan, report_path: actual_report_path) }

  it_behaves_like 'recorded report' do
    let(:recorded_report) { expected_report }
  end

  it_behaves_like 'valid report'
end
