# Secure Analyzers Integration Test

Ruby gem, scripts and Docker image to implement integration tests for the Secure analyzer projects.

This is based on [RSpec](rspec.info) and RSpec [shared examples](https://relishapp.com/rspec/rspec-core/v/3-10/docs/example-groups/shared-examples).

## Docker image

This project builds an `integration-test` Docker image where the [`gitlab_secure-integration_test` gem](#rubygem) and its dependencies are preinstalled. It includes ruby, rspec, and all distributions of the [Security Report Schemas](https://gitlab.com/gitlab-org/security-products/security-report-schemas), so that JSON schema validation can be performed without fetching them. A re-build of the base image is necessary in order to pull in the latest schemas.

The Docker image is based on the `docker` image, making it possible to run an analyzer container from the `integration-test` container, in a Docker-in-Docker configuration.

### How to run the integration test Docker container locally

When the integration test Docker image `registry.gitlab.com/gitlab-org/security-products/analyzers/integration-test:stable` is used to run the rspec tests of a source project, for example, the [gemnasium image spec](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/blob/master/spec/image_spec.rb), the integration test fetches the image pointed to by the `TMP_IMAGE` variable, and then executes it in another Docker container. Because of this, the integration test Docker container must have access to a Docker daemon.

We can use the local host's Docker daemon by mounting the host's Docker socket in the integration test container as follows:

1. Clone the project you want to run the integration tests on to your local machine:

   ```shell
   git clone https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium.git
   ```

1. (Optional) Make changes to the source project, then build and tag your image:

   ```shell
   docker build --platform linux/amd64 -f build/gemnasium/alpine/Dockerfile -t gemnasium:latest .
   ```

1. Run the [integration test](https://gitlab.com/gitlab-org/security-products/analyzers/integration-test/) image to run `rspec` against the spec files located in the project cloned in step `1.`:

   ```shell
   docker run --platform linux/amd64 -it --rm -v "$PWD:$PWD" -w "$PWD" \
     -e TMP_IMAGE=gemnasium:latest \
     -v /var/run/docker.sock:/var/run/docker.sock \
     registry.gitlab.com/gitlab-org/security-products/analyzers/integration-test:stable rspec

   .................................................................................................................................

   Finished in 1 minute 22.01 seconds (files took 0.37624 seconds to load)
   129 examples, 0 failures
   ```

   Notes about the arguments used in the above command:

   - `-v /var/run/docker.sock:/var/run/docker.sock`

      This is used for mounting the local host's Docker socket in the integration test container.

   - `TMP_IMAGE=gemnasium:latest`

      This is the URL of the image to be tested. The above command runs the integration test using the local `gemnasium:latest` Docker container against the spec files located in `$PWD/spec`. If you want to run tests using a remote GitLab analyzer image, pass `TMP_IMAGE=registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium:latest`.

### Troubleshooting

If you encounter the following error while running `docker build`:

```shell
image with reference sha256:<some-value> was found but does not match the specified platform: wanted linux/amd64, actual: linux/arm64/v8
```

your system might have cached Docker image layers that were previously built for ARM.

Running `docker system prune -a` will fix this, but be aware it will erase all of the Docker images from your computer.

## Rubygem

The `gitlab_secure-integration_test` Rubygem provides RSpec [shared examples](https://relishapp.com/rspec/rspec-core/v/3-10/docs/example-groups/shared-examples) and Ruby classes that can be used to test Secure analyzer projects.

Currently the gem isn't published, so users must either build it or use the [Docker image](#docker-image) where it's already preinstalled.

### Shared examples for scans

Shared examples for scans are used to check:
- combined output of the analyzer (stdout combined with stderr)
- exit code of the analyzer
- existence of the JSON report file

They check a `scan` object that responds to these methods:
- `combined_output` (string)
- `exit_code` (integer)
- `report_path` (string)

`report_path` is the relative path of the generated report, if any.

`DockerRunner#run` returns an object that responds to these methods.

The scan to be checked must be exposed using `let(:scan)`.

Available shared examples:
- `successful scan` is a scan that shows no error, exits with `0`, and creates a file
- `failed scan` is a scan that shows errors, exits with a non-zero exit code, and creates no file
- `crashed scan`  is a scan that behaves like a failed scan, except that it shows no error

### Shared examples for reports

Shared examples for reports are used to check the parsed generated report, obtained when parsing the JSON document with `JSON.parse`.

Available shared examples:
- `empty report` is a report with a scan and a version but no vulnerabilities
- `non-empty report` is a report with a scan, a version, and vulnerabilities
- `recorded report` is a report equivalent to a report previously recorded for the same scan input. Report comparison is implemented in the `Comparable` module.
- `valid report` is a report that passes JSON schema validation using the [Security Report Schemas](https://gitlab.com/gitlab-org/security-products/security-report-schemas)
- `non-empty CycloneDX files` ensures that CycloneDX SBOMs have been created
- `recorded CycloneDX files` ensures that CycloneDX SBOMs match the expectations
- `non-empty SBOM manifest` ensures that the SBOM manifest has been created
- `recorded SBOM manifest` ensures that the SBOM manifest matches the expectation

#### Variables used by the shared examples

- `report`
   - Purpose: contains the `JSON` of the actual report
   - Used in the following shared examples:
      - `empty report`
      - `non-empty report`
      - `recorded report`
      - `valid report`
   - Usage:

      ```ruby
      let(:report) { scan.report }
      ```

- `recorded_report`
   - Purpose: contains the `JSON` of the expected report
   - Used in the following shared examples:
      - `recorded report`
   - Usage:

      ```ruby
      def parse_expected_report(expectation_name, report_filename: "gl-dependency-scanning-report.json")
        path = File.join(expectations_dir, expectation_name, report_filename)
        JSON.parse(File.read path)
      end

      let(:recorded_report) { parse_expected_report(project) }
      ```

- `relative_sbom_paths`
   - Purpose: an array of paths to `gl-sbom-*.cdx.json` expectation files.
   - Used in the following shared examples:
      - `valid CycloneDX files`
      - `expected CycloneDX metadata tool-name`
      - `recorded CycloneDX files`
   - Usage:

      ```ruby
      let(:relative_sbom_paths) {
        ["go-project/gl-sbom-go-go.cdx.json", "javascript-project/gl-sbom-npm-npm.cdx.json",
         "ruby-project-1/gl-sbom-gem-bundler.cdx.json", "ruby-project-2/gl-sbom-gem-bundler.cdx.json"]
      }
      ```

- `relative_sbom_manifest_path`
   - Purpose: path to the `sbom-manifest.json` expectation file.
   - Used in the following shared examples:
      - `recorded SBOM manifest`
   - Usage:

      ```ruby
      let(:relative_sbom_manifest_path) { "sbom-manifest.json" }
      ```

- `relative_expectation_dir`
   - Purpose: allows providing an optional parent dir for the files specified in the `relative_sbom_paths` or `relative_sbom_manifest_path` variables. This is useful if the SBOM expectation files are located in a different directory than the `project`.
   - Used in the following shared examples:
      - `recorded SBOM manifest`
      - `recorded CycloneDX files`
   - Usage:

      ```ruby
      let(:relative_expectation_dir) { "js-npm/lockfile-v1-ignore-dev-dependencies" }
      ```

### DockerRunner

`DockerRunner` is used to run the analyzer image in a Docker container using the `docker` command, and to collect the result of the scan.

`DockerRunner.run_with_cache(image_name, fixtures_dir, target_dir, output_directory, **optional_arguments)` initializes a runner with the following arguments:

- Required arguments

   - `image_name`: Name of the analyzer image

     Example: `registry.gitlab.com/security-products/gemnasium-maven:3`

   - `fixtures_dir`: Path of the fixtures directory

     Example: `qa/fixtures`

   - `target_dir`: Path of the directory to be scanned

     Example: `qa/fixtures/multi-project/default`

   - `output_dir`: Path of the directory where the output files will be placed

     Example: `when-scanning-go-modules/behaves-like-successful-scan/creates-a-report`

     The `output_dir` value will be automatically generated and assigned to the `@output_dir` instance variable once you add `require 'gitlab_secure/integration_test/spec_helper'` to your spec file.

     **Note:** Previous versions of the `integration-test` image used `@description` for this value, however, this ended up generating long path names, such as `when-scanning-go-modules-behaves-like-successful-scan-creates-a-report`, and in some instances, the path name would exceed the maximum length allowed by the operating system and cause an error.

     In order to work around this limitation, support for `output_dir` was added, which creates a new subdirectory for each context and example. Both `@description` and `@output_dir` are currently supported, however, usage of `@description` is now deprecated and should be replaced with `@output_dir`.

- Optional arguments

   - `variables`

     A hash containing variables to be passed to the Docker container using `-e <variable-name>=<variable-value>`

     Example: `{ DS_EXCLUDED_PATHS: "/go.sum", "DS_INCLUDE_DEV_DEPENDENCIES": "false" }`

   - `mount_points`

      A hash containing `local dir` => `Docker dir` key value pairs for mounting a local directory inside the Docker container

      Example: `{ File.join(fixtures_dir, project, 'git-dir-for-autoremediation') => '$WORKDIR/.git' }`

   - `report_filename`

      The filename of the generated report. Defaults to `gl-dependency-scanning-report.json`.

   - `command`

      Array that overrides the `CMD` defined in the `Dockerfile`.

      Example: `["/analyzer", "sbom"]`

   - `script`

      Shell script to be executed in the container.

      Example:

      ```ruby
      let(:script) do
        <<~HERE
        #!/bin/sh

        export HOME=/dev/null

        /analyzer run
        HERE
      end
      ```

   - `offline`

      When set to true, the container runs without a network connection.

   - `privileged`

      When set to true, the container runs as the root user using by passing the `--user root` option.

   - `debug`

      When set to true, the logs from all docker containers are printed in the Job log.

The exact arguments and options of the `initialize` method are documented in the class.

A scan is performed when running the `run` method of the runner. This calls the `docker run` command, and returns an object that responds to `combined_output`, `exit_code`, `report`, `target_dir`, and `report_path`. The returned object can be checked using [shared examples for scans](#shared-examples-for-scans).

`DockerRunner.run_with_cache` implements in-memory caching. It is used to evaluate multiple RSpec examples without running multiple scans.

### Example

To illustrate, let's consider a Dependency Scanning analyzer project that contains:
- `spec/image_spec.rb`, an RSpec file using the `gitlab_secure-integration_test` Rubygem
- `qa/fixtures/go-modules/default`, a directory containing fixture files supported by the dependency scanning analyzer
- `qa/expect/go-modules/default/gl-dependency-scanning-report.json`, the report that's expected when scanning `qa/fixtures/go-modules/default`
- `qa/expect/go-modules/default/gl-sbom-go-go.cdx.json`, the expected CycloneDX SBOM file

The project will have a layout similar to the following:

```shell
.
└── gemnasium/
    ├── spec/
    │   └── image_spec.rb
    └── qa/
        ├── expect/
        │   └── go-modules/
        │       └── default/
        |           ├── gl-sbom-go-go.cdx.json
        │           └── gl-dependency-scanning-report.json
        └── fixtures/
            └── go-modules/
                └── default/
                    ├── go.mod
                    └── go.sum
```

Let's say that analyzer builds a temporary image, and that the image name is available in a CI variable named `TMP_IMAGE`.

The image can be tested against `qa/fixtures/go-modules/default` using this RSpec file:

```ruby
require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe "when scanning go-modules" do
  let(:project) { "go-modules/default" }
  let(:fixtures_dir) { "qa/fixtures" }
  let(:expectations_dir) { "qa/expect" }
  let(:target_dir) { File.join(fixtures_dir, project) }

  let(:scan) do
    image_name = ENV["TMP_IMAGE"]
    GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
      image_name, fixtures_dir, target_dir, @output_dir)
  end

  it_behaves_like "successful scan"

  describe "created report" do
    let(:report) { scan.report }

    it_behaves_like "non-empty report"
    it_behaves_like "valid report"

    it_behaves_like "recorded report" do
      let(:recorded_report) do
        path = File.join(expectations_dir, "go-modules/default/gl-dependency-scanning-report.json")
        JSON.parse(File.read(path))
      end
    end

    describe "CycloneDX SBOMs" do
      let(:relative_expectation_dir) { project }
      let(:relative_sbom_paths) { ["gl-sbom-go-go.cdx.json"] }

      it_behaves_like "non-empty CycloneDX files"
      it_behaves_like "recorded CycloneDX files"
      it_behaves_like "valid CycloneDX files"
    end
  end
end
```

This RSpec file is executed in a CI job that uses `integration-test` as a base image, and where Docker-in-Docker is enabled:

```yaml
# Run RSpec to check the $TMP_IMAGE image
image test:
  stage: test
  image:
    name: registry.gitlab.com/gitlab-org/security-products/analyzers/integration-test:stable
  services:
    - docker:20.10-dind
  script:
    - rspec spec/image_spec.rb
```

This way the temporary image can be tested using RSpec before being published to an official location.

After running the integration-test, the generated report files will be placed in a `tmp/test-*` directory located in the same project containing the spec files that are executed, as follows:

```shell
.
└── gemnasium/
    ├── spec/
    │   └── image_spec.rb
    ├── qa/
    │   ├── scripts/
    │   ├── expect/
    │   └── fixtures/
    └── tmp/
        └── test-63192/
            └── go-modules/
                └── default/
                    └── when-scanning-go-modules/
                        └── behaves-like-successful-scan/
                            └── creates-a-report/
                                ├── go.sum
                                ├── gl-sbom-go-go.cdx.json
                                ├── sbom-manifest.json
                                └── gl-dependency-scanning-report.json
```

## QA scripts

QA scripts check generated reports of a specific type. This is the generic check for basic properties to ensure the generated report is not empty, valid, and matches what was expected based on the recorded report. For instance, `sast-qa` checks a single SAST report.

QA scripts proceed in 3 steps:
1. load generated report and expected report using environment variables
1. compare generated report to expected report
1. perform JSON schema validation on generated report

The following table lists the QA script, along with the environment variables and the default paths they use.

| Script | Scanner type | Path of generated report | Env. var. for expected report |
| ------ | ------------ | ------------------------ | ----------------------------- |
| `dependency-scanning-qa` | Dependency Scanning | `gl-dependency-scanning-report.json` | `DS_REPORT_URL` |
| `sast-qa` | SAST | `gl-sast-report.json` | `SAST_REPORT_URL` |
| `secret-detection-qa` | Secret Detection | `gl-secret-detection-report.json` | `SECRET_DETECTION_REPORT_URL` |

QA scripts are to be used in job integration tests, in a 2-stage pipeline:
1. The first stage runs scanning jobs. These jobs upload the generated reports a artifacts.
1. The second stage runs QA jobs running QA scripts, to check the generated report.

For instance, a test project scanned by the `gemnasium-dependency_scanning` job
could have this CI configuration file to do a job integration test:

```yaml
stages:
  - test
  - qa

include:
  - template: Dependency-Scanning.gitlab-ci.yml

# override scanning job to expose the Dependency Scanning report as a CI artifact
gemnasium-dependency_scanning:
  stage: test
  artifacts:
    paths:
      - gl-dependency-scanning-report.json

# check report generated by the scanning job
qa-gemnasium-dependency_scanning:
  needs: ["gemnasium-dependency_scanning"]
  stage: qa
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/integration-test:stable
  script:
    - dependency-scanning-qa
```

The QA scripts are simple RSpec scripts using [shared examples for reports](#shared-examples-for-reports).
